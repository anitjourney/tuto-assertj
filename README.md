## Assertj

JUnit is great, it allows us to easily test our code. However, its `Assert` methods lack of readability.
A really great alternative to the assertion ambiguities and sometimes limited capabilities of `JUnit` (even if it improved a lot with JUnit 5) is [`Assertj`](http://joel-costigliola.github.io/assertj).

### Setup

If you are using `Spring Boot`, you already have `Assertj` in your classpath. Otherwise, you can add the following dependency in your maven file:
```xml
<dependency>
  <groupId>org.assertj</groupId>
  <artifactId>assertj-core</artifactId>
  <version>3.11.1</version>
  <scope>test</scope>
</dependency>
```

### Unambiguous comparison

When you write `Assert.assertEquals(a, b)`, who is the expected value and who is the actual value?
You may know it by heart, but, personally, I always have a doubt.

Instead of:

```java
import org.junit.jupiter.api.Assertions;

Assertions.assertEquals(a, b);
```

You can write:
```java
import org.assertj.core.api.Assertions;

Assertions.assertThat(b).isEqualTo(a);
```

Of course, there is a lot more than `isEqualTo()`. A few great examples are:
- startsWith(...)
- contains(...)
- endsWith(...)
- hasSize(...)
- ...

### Multiple asserts

Once you indicated the subject of the test, `Assertj` lets you test many conditions on it:
```java
import org.assertj.core.api.Assertions;

Assertions.assertThat(myText)
    .startsWith("Dear,")
    .contains("message")
    .endsWith(".")
    .hasLineCount(42)
    .doesNotContainPattern("[Pp]assword");
```

### Exceptions

In the same way you can easily test thrown exceptions with `JUnit5`, `Assertj` provides powerful mechanisms to it using lambdas:
```java
import org.assertj.core.api.Assertions;

Assertions.assertThatThrownBy(() -> { throw new Exception("boom!"); })
    .isInstanceOf(Exception.class)
    .hasMessageContaining("boom")
    .hasNoCause();
```

### Other

Many other functionalities are available can help you improve the readability of your tests and make them less frustrating to write.
I encourage you to read [this page from the Assertj website](http://joel-costigliola.github.io/assertj/assertj-core-features-highlight.html).

