package be.meulemans.assertj;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;

public class Example {

    private String a = "HEllo!";
    private String b = a;
    private String text = "Dear, \nI'm happy to say hi.";


    @Test
    public void testEquality_junit() {
        org.junit.jupiter.api.Assertions.assertEquals(b, a);
    }

    @Test
    public void testEquality_assertj() {
        Assertions.assertThat(a).isEqualTo(b);
    }

    @Test
    public void testMultiple_junit() {
        org.junit.jupiter.api.Assertions.assertTrue(text.startsWith("Dear,"));
        org.junit.jupiter.api.Assertions.assertTrue(text.contains("happy"));
        org.junit.jupiter.api.Assertions.assertTrue(text.endsWith("."));
        org.junit.jupiter.api.Assertions.assertEquals(text.split("\n").length, 2);
        org.junit.jupiter.api.Assertions.assertFalse(text.matches("[Pp]assword"));
    }

    @Test
    public void testMultiple_assertj() {
        Assertions.assertThat(text)
                .startsWith("Dear,")
                .contains("happy")
                .endsWith(".")
                .hasLineCount(2)
                .doesNotContainPattern("[Pp]assword");
    }

    @Test
    public void testException_junit() {
        org.junit.jupiter.api.Assertions.assertThrows(
                Exception.class,
                this::throwException,
                "boom");
    }

    @Test
    public void testException_assertj() {


        ThrowableAssert.ThrowingCallable aa;
        Assertions.assertThatThrownBy(this::throwException)
                .isInstanceOf(Exception.class)
                .hasMessageContaining("boom")
                .hasNoCause();
    }

    private void throwException() throws Exception {
        throw new Exception("boom!");
    }

}
